<?php

namespace App\Model;

/**
 * @author Philip Washington Sorst <philip@sorst.net>
 */
class MavenMetadata
{
    private $groupId;

    private $artifactId;

    private $version;
}
